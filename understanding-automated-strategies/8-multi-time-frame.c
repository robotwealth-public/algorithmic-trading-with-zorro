/* 
MULTIPLE TIME FRAME TRADING

*/

function run()
{
	StartDate= 20130101;
	EndDate = 20181231;
	set(PLOTNOW);
	setf(PlotMode, PL_FINE);
	MaxLong = MaxShort = 1;
	
	BarPeriod = 60;
	
	// 4-hourly trade filter
	TimeFrame = 4;
	vars PriceH4 = series(price());
	vars Filter = series(LowPass(PriceH4, 200));
	
	//Entries/Exits on 1-hourly 
	TimeFrame = 1;
	vars PriceH1 = series(price());
	vars rsi = series(RSI(PriceH1, 14));
	int overbought = 70;
	int oversold = 30;
	
	Stop = 4*ATR(100);
	TakeProfit = 2*ATR(100);
	
	if(crossOver(rsi, overbought) and PriceH1[0] < Filter[0]) enterShort();
	if(crossUnder(rsi, oversold) and PriceH1[0] > Filter[0]) enterLong();
	
	plot("Filt", Filter, MAIN, BLUE);
	plot("rsi", rsi, NEW, RED);
	plot("ob", overbought, 0, BLACK);
	plot("os", oversold, 0, BLACK);
}