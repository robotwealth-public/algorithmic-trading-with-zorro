/*
MAX - Moving Average Cross
Buy/sell upon cross of fast and slow  moving averages
Use MMI as additional filter
*/

function run()
{
	set(PARAMETERS);
	
	StartDate = 20100101;
	EndDate = 20151231;
	LookBack = 500 + UnstablePeriod;
	MaxLong = MaxShort = 1;
	
	vars Price = series(price());
	
	//set up moving averages
	int fastPeriod = optimize(50, 10, 100, 10);
	int slowPeriod = optimize(2, 1.5, 5, 0.5)*fastPeriod;
	vars maFast = series(EMA(Price, fastPeriod));
	vars maSlow = series(EMA(Price, slowPeriod));
	
	//set up Market Meanness Index
	vars mmi = series(MMI(Price, slowPeriod));
		
	//set up exits
	Stop = optimize(3, 0.5, 5, 0.5)*ATR(100);
	Trail = 2*Stop;
	
	//entry conditions
	if(crossOver(maFast, maSlow) and falling(mmi))
	{
		enterLong();		
	}
	if(crossUnder(maFast, maSlow) and falling(mmi))
	{
		enterShort();
	}
	
	//plots
	plot("maFast", maFast, MAIN, BLUE);
	plot("maSlow", maSlow, 0, RED);
	plot("mmi", mmi, NEW, BLUE);	
}