function run()
{
	set(PLOTNOW|LOGFILE);
	setf(PlotMode, PL_FINE);
	StartDate= 20100101;
	EndDate = 20181231;
	MaxLong = MaxShort = 1;
	
	vars Price = series(price());
	vars High = series(priceHigh());
	vars Low = series(priceLow());
	
	//create series from output of alligator function
	vars alligator = series(Alligator(Price));
	vars Red = series(rRed);
	vars Blue = series(rBlue);
	vars Green = series(rGreen);
	
	//record crossings of the alligator lines
	vars sigs = series(0); 
	if(crossOver(Red, Blue) or crossOver(Red, Green) or crossOver(Blue, Green) or crossOver(Blue, Red) or crossOver(Green, Red) or crossOver(Green, Blue))
	{
		sigs[0] = 1; // store the crossing as a 1 in the series	
	}
	
	// create fractal indicators
	var fHigh = FractalHigh(High, 11);
	var fLow = FractalLow(Low, 11);
	static var fH, fL;
	if(fHigh != 0) fH = fHigh;
	if(fLow != 0) fL = fLow;
	
	//trade logic
	EntryTime = 5;
	if(Sum(sigs, 5) > 2 and priceClose() < fH and NumOpenShort == 0) 
	{
		Entry = fH;
		Stop = fL;
		Trail = 0.5*(Entry - Stop);
		enterLong();		
	}
	if(Sum(sigs, 5) > 2 and priceClose() > fL and NumOpenLong == 0) 
	{
		Entry = fL;
		Stop = fH;
		Trail = 0.5*(Stop - Entry);
		enterShort();
	}
	
	plot("Recent High", fH, MAIN|DOT, BLUE);
	plot("Recent Low", fL, MAIN|DOT, RED);
	plot("red", rRed, MAIN, RED);
	plot("blue", rBlue, 0, BLUE);
	plot("green", rGreen, 0, GREEN);
	
}