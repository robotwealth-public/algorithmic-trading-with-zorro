/*
DIGITAL FILTERS

Use the turning points in a "roofing" (bandpass) filter to trade a price curve. 

*/

function run()
{
	LookBack = 260;
	set(PLOTNOW);
	setf(PlotMode, PL_FINE);
	BarPeriod = 60;
	
	StartDate= 20100101;
	EndDate = 20181231;
	
	MaxLong = MaxShort = 1;
	
	vars Price = series(price());
	vars filter = series(Roof(Price, 240, 260));
	
	if(valley(filter)) enterLong();
	if(peak(filter)) enterShort();
	
	setf(PlotMode, PL_FINE);
	plot("filter", filter, NEW, BLUE);
}