/* 
Donchian Channel Mean Reversion Strategy

*/

function run()
{
	set(PLOTNOW);
	setf(PlotMode, PL_FINE);
	StartDate = 20100101;
	EndDate = 20181231;
	MaxLong = MaxShort = 1;
	BarPeriod = 1440;
	
	int donchPeriod = 20;
	DChannel(donchPeriod);
	
	LifeTime = 12;
	
	if(priceClose() < rRealUpperBand and NumOpenShort + NumOpenLong == 0)
	{
		Entry = -rRealUpperBand;
		enterShort();
	}
	
	if(priceClose() > rRealLowerBand and NumOpenShort + NumOpenLong == 0)
	{
		Entry = -rRealLowerBand;
		enterLong();
	}

	plot("upperDchannel", rRealUpperBand, BAND1, 0x00CC00);
	plot("lowerDchannel", rRealLowerBand, BAND2, 0xCC00FF00);
}