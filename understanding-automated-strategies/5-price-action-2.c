function run()
{
	StartDate= 20100101;
	EndDate = 20181231;
	set(PARAMETERS|PLOTNOW);
	setf(PlotMode, PL_FINE);
	
	BarPeriod = 240;
	LookBack = 150;
	MaxLong = MaxShort = 1;
	
	var rangeLimit = optimize(1.5, 1.0, 2.0, 0.25)*ATR(100);
	
	EntryTime = 5;
	LifeTime = 5;
	
	if(TrueRange() > rangeLimit)
	{
		if(priceClose(0) > priceOpen(0)) // up bar
		{
			Entry = -0.618 * (priceHigh(0) - priceLow(0)); // set limit order at 0.618 fib level of previous bar
			enterLong();
		}
		else if (priceClose(0) < priceOpen(0)) // down bar
		{
			Entry = -0.618 * (priceHigh(0) - priceLow(0)); // set limit order at 0.618 fib level of previous bar
			enterShort();
		}
	}
	
}