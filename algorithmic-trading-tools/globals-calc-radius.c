var pi = 3.14159;

var calculateArea1()
{
    var radius = 3.5;
    return pi * pow(radius, 2);
}

var calculateArea2()
{
    var radius = 5.3;
    return pi * pow(radius, 2);
}

function main()
{
    var area1 = calculateArea1();
    var area2 = calculateArea2();
    printf("\nArea 1 = %f", area1);
    printf("\nArea 2 = %f", area2);
}