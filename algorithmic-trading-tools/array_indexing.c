function main()
{
	int myArray[10] = { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 };
	
	printf("\nThe first element of myArray is %d", myArray[0]);
	printf("\nThe second element of myArray is %d", myArray[1]);
	printf("\nThe third element of myArray is %d", myArray[2]);
	printf("\nThe fourth element of myArray is %d", myArray[3]);
	printf("\nThe fifth element of myArray is %d", myArray[4]);
	printf("\nThe sixth element of myArray is %d", myArray[5]);
	printf("\nThe seventh element of myArray is %d", myArray[6]);
	printf("\nThe eighth element of myArray is %d", myArray[7]);
	printf("\nThe ninth element of myArray is %d", myArray[8]);
	printf("\nThe tenth element of myArray is %d", myArray[9]);
}