void main()
{
    int i;
    int a = 500;
    int b = 1743;
    var sum = 0;
    int count = 0;
    if (a > b) printf("error: a must be smaller than b");
    else
    {
        for(i=a; i<=b; i++)
        {
            sum += i;
            count++;    
        }
        var average = sum/count;
        printf("\nAverage: %.3f", average);
    }
}