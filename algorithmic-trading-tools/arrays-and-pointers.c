function main() 
{ 
	int myArray[6] = { 1, 2, 3, 4, 5, 6 }; 
	int *myPointer; 
	myPointer = myArray; 
	printf("\nThe value of myArray[3] is %d", myArray[3]); 
	printf("\nThe value of myPointer[3] is %d", myPointer[3]); 
}